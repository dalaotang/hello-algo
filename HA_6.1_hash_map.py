# 初始化哈希表
hmap: dict = {}
# 添加操作
# 在哈希表中添加键值对（key,value)
hmap[12386] = "小哈"
hmap[15937] = "小罗"
hmap[16750] = "小算"
hmap[13276] = "小法"
hmap[10583] = "小鸭"

# 查询操作
# 向哈希表中输入键key，得到值value
name: str = hmap[15937]
# 删除操作
# 在哈希表中删除键值对（key，value）
hmap.pop(10583)

# 遍历哈希表
# 遍历键值对
for key, value in hmap.items():
    print(key, "->", value)
# 单独遍历key
for key in hmap.keys():
    print(key)
# 单独遍历value
for value in hmap.values():
    print(value)


# 键值对
class Pair:
    def __init__(self, key: int, value: int):
        self.key = key
        self.value = value


# 基于数组实现的哈希表
class ArrayHashMap:
    # 构造方法
    def __init__(self):
        # 初始化数组，包含100个桶
        self.buckets: list = [None] * 100

    # 哈希函数
    def hash_func(self, key: int) -> int:
        index = key % 100
        return index

    # 查询操作
    def get(self, key: int) -> str:
        # 先通过哈希函数获取index
        index: int = self.hash_func(key)
        pair: Pair = self.buckets[index]
        if pair is None:
            return None
        return pair.value

    # 添加操作
    def put(self, key: int, val: str):
        pair = Pair(key, val)
        index: int = self.hash_func(key)
        self.buckets[index] = pair

    # 删除操作
    def remove(self, key: int):
        index: int = self.hash_func(key)
        self.buckets[index] = None

    # 获取所有键值对
    def entry_set(self) -> list:
        result: list = []
        for pair in self.buckets:
            if pair is not None:
                result.append(pair)
        return result

    # 获取所有键
    def key_set(self) -> list:
        result = []
        for pair in self.buckets:
            if pair is not None:
                result.append(pair.key)
        return result

    # 获取所有值
    def value_set(self) -> list:
        result: list = []
        for pair in self.buckets:
            if pair is not None:
                result.append(pair.value)
        return result

    #  打印哈希表
    def print(self):
        for pair in self.buckets:
            if pair is not None:
                print(pair.key, "->", pair.value)
