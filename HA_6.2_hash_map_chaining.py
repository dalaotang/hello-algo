# 使用动态数组代替链表，实现链式地址哈希表
# 在这种设定下，哈希表（数组）包含多个桶，每个桶都是一个列表
# 当负载因子超过2/3时，将哈希表扩容至原先的2倍


# 键值对
class Pair:
    def __init__(self, key: int, value: int):
        self.key = key
        self.value = value


class HashMapChaining:
    # 构造方法
    def __init__(self):
        # 键值对数量
        self.size = 0
        # 哈希表容量
        self.capacity = 4
        # 触发扩容的负载因子阈值
        self.load_thres = 2.0 / 3.0
        # 扩容倍数
        self.extend_ratio = 2
        # 数组桶
        self.buckets = [[] for _ in range(self.capacity)]

    # 哈希函数
    def hash_func(self, key: int) -> int:
        # 用key除容量取余数的算法作为哈希函数
        return key % self.capacity

    # 计算负载因子
    def load_factor(self) -> float:
        return self.size / self.capacity

    def get(self, key: int) -> str:
        index = self.hash_func(key)
        bucket = self.buckets[index]
        for pair in bucket:
            if pair.key == key:
                return pair.val
        return None

    # 哈希表库容
    def extend(self):
        # 暂存原哈希表
        buckets = self.buckets
        # 初始化扩容后的新哈希表
        self.capacity *= self.extend_ratio
        self.buckets = [[] for _ in range(self.capacity)]
        self.size = 0
        # 将键值对从原哈希表班原值新哈希表
        for bucket in buckets:
            for pair in bucket:
                self.put(pair.key, pair.val)

    # 添加操作
    def put(self, key: int, val: str):
        # 当负载因子超过阈值时，进行扩容
        if self.load_factor() > self.load_thres:
            self.extend()
        index = self.hash_func(key)
        # 这里一定能拿到对象，因为初始化的时候，桶内所有数组都进行了初始化
        bucket = self.buckets[index]
        #  遍历桶，如果遇到指定的key，则更新对应的val并返回
        for pair in bucket:
            if pair.key == key:
                pair.val = val
                return
        # 若无该key，则将键值对添加至尾部
        pair = Pair(key, val)
        bucket.append(pair)
        self.size += 1

    def remove(self, key: int):
        index = self.hash_func(key)
        bucket = self.buckets[index]
        for pair in bucket:
            if pair.key == key:
                bucket.remove(pair)
                self.size -= 1
                break

    def print(self):
        for bucket in self.buckets:
            res = []
            for pair in bucket:
                res.append(str(pair.key) + "->" + pair.val)
            print(res)


# 开放寻址哈希表
class HashMapOpenAddressing:
    def __init__(self):
        self.size = 0
        self.capacity = 4
        self.load_thres = 2.0 / 3.0
        self.extend_ratio = 2
        self.buckets: list = [None] * self.capacity
        self.TOMESTONE = Pair(-1, "-1")

    # 哈希函数
    def hash_func(self, key: int) -> int:
        # 用key除容量取余数的算法作为哈希函数
        return key % self.capacity

    # 计算负载因子
    def load_factor(self) -> float:
        return self.size / self.capacity

    # 搜索key对应的桶的索引
    def find_bucket(self, key: int) -> int:
        index = self.hash_func(key)
        first_tombstone = -1
        # 线性探索，当遇到空桶的时候跳出
        while self.buckets[index] is not None:
            # 若遇到key，则返回对应桶的索引
            if self.buckets[index].key == key:
                # 若之前遇到了删除的标记，则将键值对移动至该索引处
                if first_tombstone != -1:
                    # 这里其实就是把非空的数据，往数组前面放
                    self.buckets[first_tombstone] = self.buckets[index]
                    self.buckets[index] = self.TOMESTONE
                    return first_tombstone
                else:
                    #  如果没有删除的标记，就返回桶索引
                    return index
            #  在遍历的过程中，如果key不是想要的key
            else:
                # 如果碰到首个删除标记
                if first_tombstone == -1 and self.buckets[index] is self.TOMESTONE:
                    # 就要记下删除标记在桶中的索引号
                    first_tombstone = index
                #  计算桶添加点索引，若越过尾部则返回头部（环形数组）
                index = (index + 1) % self.capacity
        #   若key不存在，则返回添加点的索引
        return index if first_tombstone == -1 else first_tombstone

    # 查询操作
    def get(self, key: int) -> str:
        # 搜索key对应的桶索引
        index = self.find_bucket(key)
        # 若找到键值对，则返回对应的val
        if self.buckets[index] not in [None, self.TOMESTONE]:
            return self.buckets[index].val
        # 若键值对不存在，则返回None
        return None

    def extend(self):
        bucket_tmp = self.buckets
        self.capacity *= self.extend_ratio
        # 扩容后的新哈希表
        self.buckets = [None] * self.capacity
        self.size = 0
        #  将键值对从原哈希表搬运至信哈希表
        for pair in bucket_tmp:
            if pair not in [None, self.TOMESTONE]:
                self.put(pair.key, pair.val)

    # 添加操作
    def put(self, key: int, val: str):
        if self.load_factor() > self.load_thres:
            self.extend()
        #  这个思路比较有意思，假定当前的哈希表是空的，也就是全是None的话
        # 那么find_bucket方法，就会直接定位到哈希方法后，对应的索引位置
        # 如果不是第一次添加数据，并且发生了哈希冲突（有可能是index位置被占用了)
        #
        index = self.find_bucket(key)
        #  如果键值对存在，则覆盖val并返回
        if self.buckets[index] not in [None, self.TOMESTONE]:
            self.buckets[index].val = val
            return
        # 若键值对不存在，则添加该键值对
        self.buckets[index] = Pair(key, val)
        self.size += 1

    # 删除操作
    def remove(self, key: int):
        # 搜索key对应的桶索引
        index = self.find_bucket(key)
        # 找到键值对，则用删除标记覆盖它
        if self.buckets[index] not in [None, self.TOMESTONE]:
            self.buckets[index] = self.TOMESTONE
            self.size = -1

    def print(self):
        for pair in self.buckets:
            if pair is None:
                print("None")
            elif pair is self.TOMESTONE:
                print("TOMESTONE")
            else:
                print(pair.key, "->", pair.val)
