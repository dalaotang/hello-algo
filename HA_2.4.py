# space complexity 空间复杂度


class Node:
    def __init__(self, x: int):
        self.val: int = x  # 节点值
        self.next: Node | Node = None  # 指向下一节点的引用


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def algorithm(n: int):
    a = 0  # O(1)
    b = [0] * 10000  # O(1)
    if n > 10:
        nums = [0] * n  # O(n)


def function() -> int:
    # 执行某些操作
    return 0


def loop(n: int):
    # 循环的空间复杂度为O(1),因为每轮function()都返回并释放了栈帧空间
    for _ in range(n):
        function()


def recur(n: int):
    # 递归的空间复杂度为O(n)，因为在运行过程中会同时存在n个未返回的recur()，从而占用O（n）的栈帧空间
    if n == 1:
        return
    return recur(n - 1)


#  常数阶O(1)
def constant(n: int):
    a = 0
    nums = [0] * 10000
    # 循环中的变量占用O(1)空间
    for _ in range(n):
        c = 0
    # 循环中的函数占用O(1)空间
    for _ in range(n):
        function()


#  线性阶
def linear(n: int):
    # 长度为n的列表占用O(n)空间
    nums = [0] * n
    # 长度为n的哈希表占用O(n)空间
    hmap = dict[int, str]()
    for i in range(n):
        hmap[i] = str(i)


#  线性阶（递归实现）
def linear_recur(n: int):
    print("递归 n =", n)
    if n == 1:
        return
    linear_recur(n - 1)


# 平方阶
def quadratic(n: int):
    num_matrix = [[0] * n for _ in range(n)]


# 平方阶（递归实现）
def quadratic_recur(n: int):
    if n <= 0:
        return 0
    nums = [0] * n
    return quadratic_recur(n - 1)


# 指数阶（建立满二叉树）
def build_tree(n: int):
    if n == 0:
        return None
    root = TreeNode(0)
    root.left = build_tree(n - 1)
    root.right = build_tree(n - 1)
    return root
