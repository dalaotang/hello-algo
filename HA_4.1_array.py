import random

num_arr: list = [1, 3, 2, 5, 4]


# 数组的两种初始化方式：无初始值、给定初始值。在未指定初始值的情况下
def the_array():
    arr: list = [0] * 5
    nums: list = [1, 3, 2, 5, 4]


# 随机访问
def random_access(nums: list) -> int:
    # 在区间[0, length(num)-1]中获取一个随机证书
    random_index = random.randint(0, len(nums) - 1)
    # 获取并返回随机元素
    random_num = nums[random_index]
    return random_num


print(random_access(num_arr))


# 在数组的索引index处插入元素num
def insert(nums: list, num: int, index: int):
    for i in range(len(nums) - 1, index, -1):
        # 从最后一个位置开始，把每一个元素向后挪一个位置，最后一个丢失，直到index的位置
        nums[i] = nums[i - 1]
    nums[index] = num


insert(num_arr, 100, 3)
print(num_arr)


# 删除索引index处的元素
def remove(nums: list, index: int):
    for i in range(index, len(nums) - 1, 1):
        nums[i] = nums[i + 1]


remove(num_arr, 2)
print(num_arr)


# 遍历
def traverse(nums: list):
    count = 0
    # 通过索引遍历数组
    for i in range(len(nums)):
        count += nums[i]
    # 直接遍历数组元素
    for num in nums:
        count += num
    # 同时遍历数组索引和元素
    for i, num in enumerate(nums):
        count += nums[i]
        count += num


#  在数组中查找指定元素，时间复杂度o(n)
def find(nums: list, target: int) -> int:
    for i in range(len(nums)):
        if nums[i] == target:
            return i
    return -1


# 扩展数组
def extend(nums: list, enlarge: int) -> list:
    # 初始化一个扩展长度后的数组
    res = [0] * (len(nums) + enlarge)
    # 将原数组中的所有元素复制到新数组
    for i in range(len(nums)):
        res[i] = nums[i]
    return res
