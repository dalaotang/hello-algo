# Time_Complexity 时间复杂度
# 常数阶： 尽管size可能很大，但是由于其与输入大小n无关，因此时间复杂度为O(1)
def constant(n: int) -> int:
    # 常数阶
    count = 0
    size = 10000
    for _ in range(size):
        count += 1
    return count


#  线性阶：操作数量相对于输入数据大小n以线性级别增长。线性阶通常出现在单层循环中
def linear(n: int) -> int:
    count = 0
    for _ in range(n):
        count += 1
    return count


# 线性阶：遍历数组
def array_traversal(nums: list) -> int:
    count = 0
    for num in nums:
        count += 1
    return count


#  平方阶:
def quadratic(n: int) -> int:
    count = 0
    for i in range(n):
        for j in range(n):
            count += 1
    return count


#  平方阶（冒泡排序）
def bubble_sort(nums: list) -> int:
    count = 0  # 计数器
    for i in range(len(nums) - 1, 0, -1):
        for j in range(i):
            # 如果前边的比后边的大，那么交换
            if nums[j] > nums[j + 1]:
                tmp = nums[j + 1]
                nums[j + 1] = nums[j]
                nums[j] = tmp
                count += 3
    print("冒泡排序后的数组为", nums)


arr = [1, 6, 3, 5, 4]
bubble_sort(arr)


# 指数阶（细胞分裂）—— 循环实现
def exponential(n: int) -> int:
    count = 0
    current_times = 1
    print("分裂次数为:", n)
    for _ in range(n):
        for _ in range(current_times):
            # 每次循环，就计数+1
            count += 1
        current_times = current_times * 2
    print("最后一层细胞数量:", current_times / 2)
    print("总共分裂数量为:", count)


exponential(2)


# 指数阶——递归实现
def exponential_recursion(n: int) -> int:
    if n <= 1:
        return 1
    return exponential_recursion(n - 1) + exponential_recursion(n - 1)


print("递归方式的指数阶最后一层的数量", exponential_recursion(5))


# 对数阶--循环
def logarithmic(n: int) -> int:
    times = 0
    while n > 1:
        n = n / 2
        times += 1
    return times


print("对数型循环次数;", logarithmic(8))


#  对数阶——递归
def log_recursion(n: int) -> int:
    if n <= 1:
        return 0
    return log_recursion(n / 2) + 1


print("对数递归型的次数：", log_recursion(9))


#  线性对数
def linear_log_recur(n: int) -> int:
    if n <= 1:
        return 1
    # 迭代，问题一分为二
    count = linear_log_recur(n // 2) + linear_log_recur(n // 2)
    # 当前问题内包含线性阶的操作
    for _ in range(n):
        count += 1
    return count


print("线性对数阶：", linear_log_recur(4))
