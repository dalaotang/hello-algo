from collections import deque
from HA_7_common import TreeNode
from HA_7_common import getExampleBinaryTree


# 层序遍历
def level_order(root: TreeNode) -> list:
    # 初始化队列，加入根节点
    # deque是个双端队列
    queue: deque[TreeNode] = deque()
    queue.append(root)
    # 初始化一个列表，用于保存遍历序列
    res = []
    # 当队列不为空的时候
    while queue:
        # 队列出队
        node: TreeNode = queue.popleft()
        # 保存节点值
        res.append(node.val)
        # 当左节点不为空的时候，左节点入队列
        if node.left is not None:
            queue.append(node.left)
        # 当右节点不为空的时候，右节点入队列
        if node.right is not None:
            queue.append(node.right)
    return res


# 前序遍历
def pre_order(root: TreeNode, res: []):
    if root is None:
        return
    # 访问优先级：根节点->左子树->右子树
    res.append(root.val)
    pre_order(root.left, res)
    pre_order(root.right, res)


# 中序遍历
def in_order(root: TreeNode, res: []):
    if root is None:
        return
    # 访问优先级：左子树->根节点->右子树
    in_order(root.left, res)
    res.append(root.val)
    in_order(root.right, res)


# 后序遍历
def post_order(root: TreeNode, res: []):
    if root is None:
        return
    # 访问优先级：左子树->右子树->根节点
    post_order(root.left, res)
    post_order(root.right, res)
    res.append(root.val)


tree = getExampleBinaryTree()
arr = level_order(tree)
print("层级遍历的结果:", arr)

pre_order_value = []
pre_order(tree, pre_order_value)
print("前序遍历的结果:", pre_order_value)

in_order_value = []
in_order(tree, in_order_value)
print("中序遍历的结果:", in_order_value)

post_order_value = []
post_order(tree, post_order_value)
print("后续遍历的结果:", post_order_value)
