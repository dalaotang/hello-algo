# 链表节点类
class ListNode:
    def __init__(self, val: int):
        self.val: int = val
        self.next: ListNode | None = None


def linkedlist_init() -> ListNode:
    # 初始化各个节点
    n0 = ListNode(1)
    n1 = ListNode(3)
    n2 = ListNode(2)
    n3 = ListNode(5)
    n4 = ListNode(4)
    # 构造节点之间的引用
    n0.next = n1
    n1.next = n2
    n2.next = n3
    n3.next = n4
    return n0
    # 我们通常将头节点当作链表的代称，比如以上代码中的链表可记作链表 n0


# 在链表节点n0之后，插入节点p
def insert(n0: ListNode, p: ListNode):
    n1 = n0.next
    p.next = n1.next
    n0.next = p


#  删除链表的节点n0之后的首个节点
def remove(n0: ListNode):
    # 如果n0没有下一个节点，那么就终止
    if not n0.next:
        return
    p = n0.next
    n0.next = p.next


# 访问链表中索引为index的节点，head为目标链表，也为链表中的第一个元素
def access(head: ListNode, index: int) -> ListNode:
    for _ in range(index):
        # 如果head为空，则终止
        if not head:
            return None
        head = head.next
    return head


#  在链表中查询值为target的首个节点
def find(head: ListNode, target: int) -> int:
    index = 0
    # 当head不为null
    while head:
        if head.val == target:
            return index
        head = head.next
        index += 1
    return -1
