from collections import deque


# 链表节点类
class ListNode:
    def __init__(self, val: int):
        self.val: int = val
        self.next: ListNode | None = None


# 初始化队列
que: deque = deque()
# 元素入队
que.append(1)
que.append(3)
que.append(2)
que.append(5)
que.append(4)
# 访问队首元素
front: int = que[0]
# 元素出队列
pop: int = que.popleft()
# 获取队列长度
size: int = len(que)
# 判断队列是否为空
is_empty: bool = len(que) == 0


class LinkedListQueue:
    # 构造函数
    def __init__(self):
        self._front: ListNode = None
        self._rear: ListNode = None
        self._size: int = 0

    # 获取队列的长度
    def size(self) -> int:
        return self._size

    # 判断队列是否为空
    def is_empty(self) -> bool:
        return self._size == 0

    # 入队列
    def push(self, num: int):
        # 在尾结点后添加num
        node = ListNode(num)
        # 如果队列为空，则令头、尾节点都指向该节点
        if self._front is None:
            self._front = node
            self._rear = node
        # 如果队列不为空，则将该节点添加至尾后节点
        else:
            self._rear.next = node
            self._rear = node
        self._size += 1

    # 出队列
    def pop(self) -> int:
        num = self.peek()
        self._front = self._front.next
        self._size -= 1
        return num

    # 访问队首元素
    def peek(self) -> int:
        if self.is_empty():
            raise IndexError("队列为空")
        return self._front.val

    # 转换成列表用于打印
    def to_list(self)-> list:
        queue = []
        temp = self._front
        while temp:
            queue.append(temp.val)
            temp = temp.next
        return queue
