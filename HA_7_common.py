class TreeNode:
    def __init__(self, val: int):
        self.val: int = val
        self.left: TreeNode = None
        self.right: TreeNode = None

def getExampleBinaryTree():
    node1 = TreeNode(val=1)
    node2 = TreeNode(val=2)
    node3 = TreeNode(val=3)
    node4 = TreeNode(val=4)
    node5 = TreeNode(val=5)
    node6 = TreeNode(val=6)
    node7 = TreeNode(val=7)
    node1.left = node2
    node1.right = node3
    node2.left = node4
    node2.right = node5
    node3.left = node6
    node3.right = node7
    return node1
