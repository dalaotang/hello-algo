time = 10


# 1 +2 + ... + 10
def for_loop(n: int) -> int:
    res = 0
    # range 左闭右开
    for i in range(1, n + 1):
        res = res + i
    return res


print("for循环迭代的结果:", for_loop(time))


def while_loop(n: int) -> int:
    res = 0
    i = 1
    while i <= n:
        res = res + i
        i += 1
    return res


print("while循环迭代的结果:", while_loop(time))


def nested_for_loop(n: int) -> str:
    res = ""
    for i in range(1, n + 1):
        for j in range(1, n + 1):
            res += f"({i},{j}),"
    return res


print("嵌套调用:", nested_for_loop(time))


# 递归
def recursion(n: int) -> int:
    if n == 1:
        return 1
    res = recursion(n - 1)
    return n + res


print("递归调用的结果：", recursion(time))


def tail_recursion(n: int, tail_recursion_result: int) -> int:
    # 尾递归
    # 终止条件, 直接返回结果
    if n == 0:
        return tail_recursion_result
    return tail_recursion(n - 1, tail_recursion_result + n)


tail_recursion_start = 3
result = 0
print("尾递归的值为：", tail_recursion(tail_recursion_start, result))


def fib(n: int) -> int:
    # 斐波那契数列
    # 终止条件f(1) = 0, f(2) = 1
    if n == 1 or n == 2:
        return n - 1
    # 递归调用 f(n) = f(n-1) + f(n-2)
    res = fib(n - 1) + fib(n - 2)
    return res


# 0 1 1 2 3 5 8
print("斐波那契数列:fib(7)=", fib(7))


def for_loop_recur(n: int) -> int:
    stack = []
    res = 0
    #  递：递归调用
    for i in range(n, 0, -1):
        stack.append(i)
    #  归： 返回结果
    while stack:
        # 通过出栈模拟了归的操作
        res = stack.pop()
    return res
