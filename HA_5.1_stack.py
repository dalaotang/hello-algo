# 初始化栈
stack: list = []
# 元素入栈
stack.append(1)
stack.append(3)
stack.append(2)
stack.append(5)
stack.append(4)

# 访问栈顶元素
peek: int = stack[-1]

# 元素出栈
pop: int = stack.pop()
# 获取栈的长度
size: int = len(stack)
# 判断是否为空
is_empty: bool = len(stack) == 0


# 链表节点类
class ListNode:
    def __init__(self, val: int):
        self.val: int = val
        self.next: ListNode | None = None


# 基于链表实现的栈
class LinkedListStack:
    def __init__(self):
        self._peek: ListNode | None = None
        self._size: int = 0

    def size(self) -> int:
        return self._size

    def is_empty(self) -> bool:
        return self._size == 0

    # 访问栈顶元素
    def peek(self) -> int:
        if self.is_empty():
            raise IndexError("栈为空")
        return self._peek.val

    # 入栈
    def push(self, val: int):
        node = ListNode(val)
        # 将新的节点的下一节点，指向原来的栈顶元素
        node.next = self._peek
        # 栈顶指向当前节点
        self._peek = node
        # 深度+1
        self._size += 1

    # 出栈
    def pop(self) -> int:
        num = self.peek()
        self._peek = self._peek.next
        self._size -= 1
        return num

    def to_list(self) -> list:
        arr = []
        node = self._peek
        while node:
            arr.append(node.val)
            node = node.next
        arr.reverse()
        return arr


class ArrayStack:
    # 构造方法
    def __init__(self):
        self._stack: list = []

    # 获取栈长度
    def size(self) -> int:
        return len(self._stack)

    # 判断栈是否为空
    def is_empty(self) -> bool:
        return self.size() == 0

    # 入栈
    def push(self, item: int):
        self._stack.append(item)

    # 出栈
    def pop(self) -> int:
        if self.is_empty():
            raise IndexError("栈为空")
        return self._stack.pop()

    # 访问元素
    def peek(self) -> int:
        if self.is_empty():
            raise IndexError("栈为空")
        return self._stack[-1]

    def to_list(self) -> list:
        return self._stack


