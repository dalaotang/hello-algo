#  列表类
class MyList:
    # 构造方法
    def __init__(self):
        #  列表容量
        self._capacity: int = 10
        # 存储数据的数组
        self._arr: list = [0] * self._capacity
        # 列表长度
        self._size: int = 0
        # 扩容倍数
        self._extend_ratio: int = 2

    # 获取列表长度
    def size(self) -> int:
        return self._size

    # 获取列表容量
    def capacity(self) -> int:
        return self._capacity

    # 访问元素
    def get(self, index: int) -> int:
        # 索引如果越界.抛出异常
        if index < 0 or index >= self._size:
            raise IndexError("索引越界")
        return self._arr[index]

    # 更新元素
    def set(self, num: int, index: int):
        if index < 0 or index >= self._size:
            raise IndexError("索引越界")
        self._arr[index] = num

    # 列表扩容
    def extend_capacity(self):
        # 新建一个长度为原数组 _extend_ratio倍的新数组，并将原数组复制到新数组
        self._arr = self._arr + [0] * self.capacity() * (self._extend_ratio - 1)
        # 更新列表容量
        self._capacity = len(self._arr)

    # 在尾部添加元素
    def add(self, num: int):
        # 元素数量超过容量是.出发扩容机制
        if self._size() == self.capacity():
            self.extend_capacity()
        self._arr[self._size] = num
        self._size += 1

    # 在中间插入元素
    def insert(self, num: int, index: int):
        if index < 0 or index >= self._size:
            raise IndexError("索引越界")
        # 元素数据容量超出容量时，出发扩容机制
        if self._size == self.capacity():
            self.extend_capacity()
        # 将索引index以及以后得元素都向后移动一位
        for j in range(self._size - 1, index - 1, -1):
            self._arr[j + 1] = self._arr[j]
        self._arr[index] = num
        # 更新元素数量
        self._size += 1

    # 删除元素
    def remove(self, index: int) -> int:
        if index < 0 or index >= self._size:
            raise IndexError("索引越界")
        # 要被删除的元素
        num = self._arr[index]
        # 将索引index后的元素都向前移动1位
        for j in range(index, self._size - 1):
            self._arr[j] = self._arr[j + 1]
        self._size -= 1
        return num
