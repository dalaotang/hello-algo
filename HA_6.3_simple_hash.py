# 加法哈希：对输入的每个字符的ASCII码进行相加，将得到的总和作为哈希值
def add_hash(key: str) -> int:
    hash = 0
    modulus = 1000000007
    for c in key:
        # ord方法是会返回字符的unicode码
        hash += ord(c)
        return hash % modulus


# 乘法哈希：利用乘法的不相关性，每轮乘以一个常数，将各个字符的ASCII码累计到哈希值中
def mul_hash(key: str) -> int:
    hash = 0
    modulus = 1000000007
    for c in key:
        hash = 31 * hash + ord(c)
    return hash % modulus


# 异或哈希：将输入数据的每个元素通过异或操作累积到一个哈希值中
def xor_hash(key: str) -> int:
    hash = 0
    modulus = 1000000007
    for c in key:
        hash ^= ord(c)
    return


# todo:这个我真的看不懂
# 旋或哈希：将每个字符的ASCII码累积到一个哈希值中，每次累积之前都会对哈希进行旋或操作
def rot_hash(key: str) -> int:
    hash = 0
    modulus = 1000000007
    for c in key:
        hash = (hash << 4) ^ (hash >> 28) ^ ord(c)
    return hash % modulus
