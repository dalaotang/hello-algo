import HA_7_common as common


# 数组表示下的二叉树类
class ArrayBinaryTree:
    # 构造方法
    def __init__(self, arr: list):
        self._tree = list(arr)

    # 列表容量
    def size(self):
        return len(self._tree)

    # 获取列表索引为i的值
    def val(self, i: int) -> int:
        # 若索引越界，则返回None，代表空位
        if i < 0 or i >= self.size():
            return None
        return self._tree[i]

    # 获取索引为i节点的左子节点的索引
    def left(self, i: int) -> int:
        return 2 * i + 1

    # 获取索引为i节点的右子节点的索引
    def right(self, i: int) -> int:
        return 2 * i + 2

    # 获取索引为i节点的父节点的索引
    def parent(self, i: int) -> int:
        return (i - 1) // 2

    # 层序遍历
    def level_order(self) -> list:
        self.res = []
        # 直接遍历数组
        for i in range(self.size()):
            if self.val(i) is not None:
                self.res.append(self.val(i))
        return self.res

    #  深度优先遍历
    def dfs(self, i: int, order: str):
        if self.val(i) is None:
            return
        # 前序遍历
        if order == "pre":
            self.res.append(self.val(i))
        self.dfs(self.left(i), order)
        # 中序遍历
        if order == "in":
            self.res.append(self.val(i))
        self.dfs(self.right(i), order)
        # 后续遍历
        if order == "post":
            self.res.append(self.val(i))
        return

    # 前序遍历
    def pre_order(self) -> list:
        self.res = []
        self.dfs(0, order="pre")
        return self.res

    # 中序遍历
    def in_order(self) -> list:
        self.res = []
        self.dfs(0, order="in")
        return self.res

    # 后序遍历
    def post_order(self) -> list:
        self.res = []
        self.dfs(0, order="post")
        return self.res


# if __name__ == '__main__':
tree = [1, 2, 3, 4, None, 6, 7, 8, 9, None, None, 12, None, None, 15]
print("使用数组表示的二叉树，其原始数组为：", tree)
arrBinTree = ArrayBinaryTree(tree)
# 层序遍历
# result =  arrBinTree.level_order()
# print("层序遍历的结果为:",result)
# 前序遍历
# result = arrBinTree.pre_order()
# print("深度优先，前序遍历结果为：", result)
# 中序遍历
# result = arrBinTree.in_order()
# print("深度优先，中遍历结果为：", result)
# 后续遍历
result = arrBinTree.post_order()
print("深度优先，后遍历结果为：", result)
